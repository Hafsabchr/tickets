# Resource Block: Terraform Backend Configuration
terraform {
  backend "s3" {
    bucket = "mybucket-hb"  # the name of the S3 bucket that was created 
    key    = "Dev/Terraform-state"      # the path to the terraform.tfstate file stored inside the bucket
    region = "us-east-2"
  }
}
