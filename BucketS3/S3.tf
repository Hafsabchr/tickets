# Resource Block: Create AWS S3 Bucket
resource "aws_s3_bucket" "mybucket-HB" {
  bucket = "mybucket-HB"
  acl    = "private"

  versioning {
    enabled = true
  }
}
