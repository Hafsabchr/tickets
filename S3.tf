# Resource Block: Create AWS S3 Bucket
resource "aws_s3_bucket" "mybucket-hb" {
  bucket = "mybucket-hb"
  acl    = "private"

  versioning {
    enabled = true
  }
}
